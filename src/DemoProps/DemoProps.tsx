import React from "react";
import UserInfo from "./UserInfo";
import { I_User } from "./type";

let dataUser: I_User = {
  id: 1,
  name: "alice",
};

export default function DemoProps() {
  return (
    <div>
      <UserInfo info={dataUser} />
    </div>
  );
}

// tsrfc
