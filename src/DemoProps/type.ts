export interface I_User {
  id: number;
  name: string;
}
export interface I_PropsUserInfo {
  info: I_User;
}

// https://wowcv.vietnamworks.com/
