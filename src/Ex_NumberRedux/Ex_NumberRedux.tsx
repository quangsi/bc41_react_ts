import React from "react";
import { useDispatch, useSelector } from "react-redux";
// import {} from "reare "
import * as NumberActionCreator from "./redux/action/numberAction";
import { bindActionCreators } from "redux";
export default function Ex_NumberRedux() {
  console.log(
    "🚀 - file: Ex_NumberRedux.tsx:5 - NumberActionCreator",
    NumberActionCreator
  );
  let dispatch = useDispatch();
  // let tangSoLuongAction = "alice";
  let { tangSoLuongAction: handleTangSoLuong } = bindActionCreators(
    NumberActionCreator,
    dispatch
  );
  // interceptor
  let number = useSelector((state: any) => {
    return state.numberReducer.soLuong;
  });
  return (
    <div className="flex items-center justify-center">
      <button
        onClick={() => {
          handleTangSoLuong(100);
        }}
        className="bg-red-700 px-5 py-2 rounded"
      >
        Giảm
      </button>
      <span className="text-xl font-bold mx-10">{number}</span>
      <button className="bg-green-700 px-5 py-2 rounded">Tăng</button>
    </div>
  );
}

// create snippet vscode
// create alias window/macos
