import { Dispatch } from "redux";
import { Action } from "../../interfaces";
import { NumberType } from "../contant/numberConant";

// export const tangSoLuongAction = (value: number) => {
//   return (dispatch: Dispatch<Action>) => {
//     dispatch({
//       type: NumberType.TANG,
//       payload: value,
//     });
//   };
// };
export const tangSoLuongAction = (value: number) => {
  return {
    type: NumberType.TANG,
    payload: value,
  };
};
