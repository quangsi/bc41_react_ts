import { combineReducers } from "redux";
import { numberREducer as numberReducer } from "./numberReducer";

export let rootReducer = combineReducers({
  numberReducer,
});
