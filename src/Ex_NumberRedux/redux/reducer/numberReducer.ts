import { Action } from "../../interfaces";
import { NumberType } from "../contant/numberConant";

interface I_InitialState {
  soLuong: number;
}
let initailState: I_InitialState = {
  soLuong: 100,
};

export const numberREducer = (
  state: I_InitialState = initailState,
  aciton: Action
) => {
  switch (aciton.type) {
    case NumberType.TANG: {
      state.soLuong += aciton.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
