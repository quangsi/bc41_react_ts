import { NumberType } from "./redux/contant/numberConant";

interface I_TangSoLuongAction {
  type: NumberType.TANG;
  payload: number;
}

interface I_GianSoLuongAction {
  type: NumberType.GIAM;
  payload: number;
}

export type Action = I_TangSoLuongAction | I_GianSoLuongAction;
