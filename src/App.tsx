import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import Ex_TodoList from "./Ex_TodoList/Ex_TodoList";
import Ex_NumberRedux from "./Ex_NumberRedux/Ex_NumberRedux";

function App() {
  return (
    <div className="App">
      {/* <DemoProps /> */}
      {/* <Ex_TodoList /> */}
      <Ex_NumberRedux />
    </div>
  );
}

export default App;
