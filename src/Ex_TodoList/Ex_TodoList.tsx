import React, { useState } from "react";
import FormTodo from "./FormTodo";
import ListTodo from "./ListTodo";
import { I_Todo } from "./types";

export default function Ex_TodoList() {
  const [todos, setTodos] = useState<I_Todo[]>([
    {
      id: 1,
      text: "Làm capstone movie nha",
      isCompleted: false,
    },
    {
      id: 2,
      text: "Làm CV đi nha",
      isCompleted: false,
    },
    {
      id: 3,
      text: "Làm dự án cuối khoá",
      isCompleted: true,
    },
  ]);
  let handleRemove = (idTodo: number) => {
    let newTodos = todos.filter((todo) => {
      return todo.id != idTodo;
    });
    setTodos(newTodos);
  };
  let handleAdd = (todo: I_Todo) => {
    const newTodos = [...todos, todo];
    setTodos(newTodos);
  };
  return (
    <div className="container mx-auto space-y-10">
      <FormTodo handleAdd={handleAdd} />
      <ListTodo handleRemove={handleRemove} todos={todos} />
    </div>
  );
}
