import React, { useState } from "react";
import { I_PropsFormTodo, I_Todo } from "./types";
const { customAlphabet } = require("nanoid");
type Props = {};

export default function FormTodo({ handleAdd }: I_PropsFormTodo) {
  const [todoTitle, setTodoTitle] = useState<string>("");
  let handleOnchangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTodoTitle(e.target.value);
  };
  let handleCreate = () => {
    const alphabet = "0123456789";
    const nanoid = customAlphabet(alphabet, 3);
    let newTodo: I_Todo = {
      id: nanoid() * 1,
      text: todoTitle,
      isCompleted: false,
    };
    handleAdd(newTodo);
    // nanoid npm
  };
  return (
    <div className="flex">
      <input
        value={todoTitle}
        onChange={handleOnchangeTitle}
        type="text"
        className="px-5 py-2 border-2 rounded border-gray-600 grow "
      />
      <button
        onClick={handleCreate}
        className="bg-orange-500 px-5 py-2 text-white rounded"
      >
        Add Todo
      </button>
    </div>
  );
}
