import React from "react";
import { I_PropsListTodo } from "./types";
import ItemTodo from "./ItemTodo";

type Props = {};

export default function ListTodo({ todos, handleRemove }: I_PropsListTodo) {
  console.log(todos);

  return (
    <div className="relative overflow-x-auto">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              ID
            </th>
            <th scope="col" className="px-6 py-3">
              Title
            </th>
            <th scope="col" className="px-6 py-3">
              Is Completed
            </th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {todos.map((todo) => {
            return <ItemTodo todo={todo} handleDelete={handleRemove} />;
          })}
        </tbody>
      </table>
    </div>
  );
}
