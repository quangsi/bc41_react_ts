export interface I_Todo {
  id: number;
  text: string;
  isCompleted: boolean;
}

export interface I_PropsListTodo {
  todos: I_Todo[];
  handleRemove: (id: number) => void;
}

export interface I_PropsItemTodo {
  todo: I_Todo;
  handleDelete: (id: number) => void;
}

export interface I_PropsFormTodo {
  handleAdd: (toto: I_Todo) => void;
}
