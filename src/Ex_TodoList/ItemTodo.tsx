import React from "react";
import { I_PropsItemTodo } from "./types";

type Props = {};

export default function ItemTodo({ todo, handleDelete }: I_PropsItemTodo) {
  return (
    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
      <th
        scope="row"
        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
      >
        {todo.id}
      </th>
      <td className="px-6 py-4">{todo.text}</td>
      <td className="px-6 py-4">
        <input
          className="scale-150"
          type="checkbox"
          checked={todo.isCompleted}
        />
      </td>
      <td>
        <button
          onClick={() => {
            handleDelete(todo.id);
          }}
          className="w-5 h-5 rounded bg-red-600 text-white"
        >
          x
        </button>
      </td>
    </tr>
  );
}
